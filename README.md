# Awesome Android Apps

A curated list of awesome open source Android applications, tutorials and resources. Inspired by other awesome-\* projects.

[<img src="logo.png">](https://github.com/LinuxCafeFederation/awesome-android)

## Contents

- [Advertisement blocking](#advertisement-blocking)
- [App stores](#app-stores)
- [Basic Utilities](#basic-utilities)
- [Book readers](#book-readers)
- [Browsers](#browsers)
- [Cameras](#cameras)
- [Communication](#communication)
- [Education](#education)
- [Games](#games)
- [Media Players](#media-players)
- [Music](#music)
- [Office](#office)
- [Password managers](#password-managers)
- [Personalisation](#personalisation)
- [Photos](#photos)
- [Productivity](#productivity)
- [Streaming](#streaming)
- [VPN](#vpn)
- [Others](#others)
- [Tools](#tools)
- [Modding](#modding)

## Advertisement blocking

- [AdAway](https://adaway.org/) - Ad blocker for Android using the hosts file (Root permission is optional but it is recommended).
- [Blokada](https://blokada.org/) - Ad blocker for Android using the VPN API.
- [DNSfilter](https://www.zenz-solutions.de/personaldnsfilter/) - Ad blocker for Android using a VPN, supports hosts files.

## App stores

- [Aurora Droid](https://gitlab.com/AuroraOSS/auroradroid) - Fork of the F-Droid client with external repos ready to sync (Root permission is optional).
- [Aurora Store](https://auroraoss.com/) - Unofficial FOSS client to Google Play Store (Root permission is optional).
- [Foxy Droid](https://github.com/kitsunyan/foxy-droid) - Yet another F-Droid client.

## Basic Utilities

- [Koler](https://github.com/Chooloo/koler) - A uniquely stylized phone app, with customizable features - for Android.
- [Simple Calculator](https://f-droid.org/en/packages/com.simplemobiletools.calculator/) - A calculator for your quick calculations.
- [Simple Calendar](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/) - Be notified of the important moments in your life.
- [Simple Clock](https://f-droid.org/en/packages/com.simplemobiletools.clock/) - A combination of a clock, alarm, stopwatch and timer.
- [Simple Contacts](https://f-droid.org/en/packages/com.simplemobiletools.contacts.pro/) - A premium app for contact management with no ads, supports groups and favorites.
- [Simple Dialer](https://f-droid.org/en/packages/com.simplemobiletools.dialer/) - A handy phone call manager with phonebook, number blocking and multi-SIM support.
- [Simple Flashlight](https://f-droid.org/en/packages/com.simplemobiletools.flashlight/) - A simple flashlight without ads.
- [Simple Voice Recorder](https://f-droid.org/en/packages/com.simplemobiletools.voicerecorder/) - An easy way of recording any discussion or sounds without ads or internet access.

## Book readers

- [Book Reader](https://gitlab.com/axet/android-book-reader/tree/HEAD) - Based on [FBReader](https://fbreader.org/); book reader with a simple UI.
- [Librera PRO](https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader/) - Librera Reader is a highly customizable and feature-rich application
  for reading e-books in PDF, EPUB, MOBI, DjVu, FB2, TXT, RTF, AZW, AZW3, HTML, CBZ, CBR, DOC, DOCX, etc. formats. Note that Playstore version can access non-free services like Dropbox, Google Drive and Onedrive while the one mentioned here(F-droid version) can't.
- [Neko](https://github.com/CarlosEsco/Neko) - Free, open source, unofficial MangaDex reader for Android.
- [Shosetsu](https://github.com/ShosetsuOrg/android-app) - Light novel reader.
- [Tachiyomi](https://github.com/inorichi/tachiyomi) - Manga reader.

## Browsers

- [Bromite](https://www.bromite.org/) - Based on Chromium; built-in adblocking; enhanced privacy; degoogled.
- [Fennec F-Droid](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/) - Browser based on the latest Firefox release; removed any proprietary bits of the standard Firefox for Android - NOTE: There might still be some binaries left and the app (or some builds) might get removed or re-pushed anytime.
- [Firefox Focus](https://support.mozilla.org/en-US/kb/focus) - Privacy-oriented browser with tracking protection and content blocking.
- [Firefox Klar](https://support.mozilla.org/en-US/kb/what-firefox-klar-android) - Privacy-oriented browser with tracking protection and content blocking; for the German market.
- [Kiwi Browser](https://kiwibrowser.com/) - Based on Chrome Canary; extension support; built-in dark mode; degoogled.
- [Privacy Browser](https://www.stoutner.com/privacy-browser/) - Minimal browser focused for privacy.
- [SmartCookieWeb](https://github.com/CookieJarApps/SmartCookieWeb) - A Secure, Free and Open Source WebView-based web browser for Android. A fork of Lightning Browser.
- [Tor Browser](https://torproject.org) - Tor browser for Android, based on FireFox.

## Cameras

- [Open Camera](https://opencamera.sourceforge.io/) - Camera app with DSLR features.
- [Simple Camera](https://f-droid.org/en/packages/com.simplemobiletools.camera/) - A camera with flash, zoom and no ads.

## Communication

- [AnySoftKeyboard](https://anysoftkeyboard.github.io/) - Keyboard with tons of features.
- [Barinsta](https://github.com/austinhuang0131/barinsta) - Open-source alternative Instagram client on Android.
- [Briar](https://code.briarproject.org/briar/briar) - Secure P2P Messaging, Anywhere.
- [Conversations](https://github.com/siacs/Conversations) - XMPP/Jabber client.
- [Delta Chat](https://github.com/deltachat/deltachat-android) - Email-based instant messaging for Android.
- [Element Android](https://github.com/vector-im/element-android) - A glossy Matrix collaboration client for Android.
- [Fedilab](https://fedilab.app/) - Fedilab is a multifunctional Android client to access the distributed Fediverse. It supports Mastodon, Pleroma, Pixelfed, Peertube, GNU Social, Friendica.
- [FlorisBoard](https://github.com/florisboard/florisboard) - FlorisBoard is a free and open-source keyboard for Android 6.0+ devices. It aims at being modern, user-friendly and customizable while fully respecting your privacy. Currently in alpha/early-beta state.
- [FluffyChat](https://gitlab.com/ChristianPauly/fluffychat-flutter) - A minimalist matrix client for Android.
- [Frost for Facebook](https://allanwang.github.io/Frost-for-Facebook/) - Third-party app for Facebook.
- [Infinity for Reddit](https://github.com/Docile-Alligator/Infinity-For-Reddit) - This is a Reddit client on Android written in Java. It does not have any ads and it features clean UI and smooth browsing experience.
- [Jitsi Meet](https://jitsi.org/jitsi-meet/) - Instant video conferences efficiently adapting to your scale.
- [KDE Connect](https://community.kde.org/KDEConnect) - A project that enables all your devices to communicate with each other.
- [Langis](https://langis.cloudfrancois.fr/) - A degoogled version of Signal Messenger app. The source of patches used to build this degoogled version is available [here](https://git.legeox.net/capslock/signal-gcm-less).
- [Neko X](https://github.com/NekoX-Dev/NekoX) - Another FOSS Telegram client (based on Telegram FOSS and Nekogram which was earlier FOSS) with more features and different variants (check release page on GitHub).
- [OpenBoard](https://github.com/dslul/openboard) - Fork of AOSP Keyboard; minimal and simple to use.
- [QKSMS](https://github.com/moezbhatti/qksms) - SMS Messaging app.
- [RedReader for Reddit](https://github.com/QuantumBadger/RedReader) - Unofficial Reddit client; material-design; ad-free and has tons of customisation.
- [RTranslator](https://github.com/niedev/RTranslator) - Universal translator based on Google's APIs and Bluetooth LE.
- [Simple SMS Messenger](https://f-droid.org/en/packages/com.simplemobiletools.smsmessenger/) - An easy and quick way of managing SMS and MMS messages without ads.
- [Slide](https://github.com/ccrama/Slide) - Unofficial Reddit client; material-design; ad-free and has tons of customisation.
- [Syphon](https://f-droid.org/en/packages/org.tether.tether/) - A privacy centric matrix client.
- [Telegram-FOSS](https://github.com/Telegram-FOSS-Team/Telegram-FOSS) - Telegram is a messaging app with a focus on speed and security. It is simple and free. Note that this FOSS version isn't official, official version can be found on Playstore but that version uses non-free dependencies. Also, the server of Telegram is closed-source [for now](https://telegram.org/faq#q-why-not-open-source-everything).
- [Tusky](https://tusky.app/) - Mastodon client for Android.
- [Twidere](https://github.com/TwidereProject/Twidere-Android) - An Open Source, fully featured Twitter/Mastodon/StatusNet/Fanfou app.
- [Yaxim](https://yaxim.org/) - Yet another XMPP instant messenger.

## Education

- [AnkiDroid](https://github.com/ankidroid/Anki-Android) - Anki flashcards on Android.
- [phyphox](https://phyphox.org/) - Sensors and data analyser.

## Games

- [Andor's Trail](https://f-droid.org/en/packages/com.gpl.rpg.AndorsTrail/) - Quest-driven Roguelike fantasy dungeon crawler RPG with a powerful story.
- [Mindustry](https://anuke.itch.io/mindustry) - Mindustry is a hybrid tower-defense sandbox factory game.
- [Minetest](https://www.minetest.net/) - An open source voxel game engine. Basically, a FOSS alternative of Minecraft.
- [UnCiv](https://github.com/yairm210/Unciv) - Open-source Android/Desktop remake of Civ V.
- [Xeonjia](https://gitlab.com/DeepDaikon/Xeonjia) - Solve ice puzzles and defeat enemies in an RPG world.

## Media Players

- [mpv](https://mpv.io/) - Minimal media player.
- [VLC](https://www.videolan.org/) - Minimal media player.

## Music

- [Metro](https://github.com/MuntashirAkon/Metro) - Fork of [Retro Music Player](#retro-music-player) with removed proprietary Google Play libraries.
- [Music](https://github.com/MaxFour/Music-Player) - Lightweight and Material Music Player.
- [Music Player GO](https://github.com/enricocid/Music-Player-GO) - A simple yet fully-featured local music player aiming at simplicity and performance.
- [Odyssey Music Player](https://github.com/gateship-one/odyssey) - Lightweight music player.
- [Retro Music Player](https://github.com/h4h13/RetroMusicPlayer) - Local music player for Android.
- [Shuttle Music Player](https://github.com/timusus/Shuttle) - Local music player for Android.
- [Simple Music PLayer](https://f-droid.org/en/packages/com.simplemobiletools.musicplayer/) - A clean music player with a customizable widget.
- [Vinyl Music Player](https://github.com/AdrienPoupa/VinylMusicPlayer) - Local music player for Android.
- [VLC](https://www.videolan.org/) - Minimal media player.

## Office

- [Collabora Office](https://gerrit.libreoffice.org/plugins/gitiles/online) - Office suite available for Android, GNU/Linux, iOS, macOS, Windows.

## Password managers

- [andOTP](https://github.com/andOTP/andOTP) - Open source two-factor authentication for Android.
- [Bitwarden](https://github.com/bitwarden/mobile) - Free and open-source, cross-platform password manager available via cloud/self-hosting. NOTE: To download on F-Droid you need to add the Bitwarden repository. Manual vault syncing is required with that version.
- [Keepass2Android](https://github.com/PhilippC/keepass2android) - Keepass2Android is an open source password manager compatible with KeePass (v1 and v2), KeePassXC, MiniKeePass and many other KeePass ports.
- [Master Password](https://masterpassword.app/) - A stateless password management solution. Uses a cryptographic algorithm calculates your site's password for you, only when you need it.
- [KeePassDX](https://www.keepassdx.com) - FOSS password manager compatible with .kdb and .kdbx files version 1-4 and a majority of KeePass programs.

## Personalisation

- [App Launcher](https://f-droid.org/en/packages/com.simplemobiletools.applauncher/) - A simple holder for your favourite app launchers.
- [Lawnchair 2](https://lawnchair.app/) - Continuation of Lawnchair 1; Pixel features; fork of [Launcher3](https://github.com/amirzaidi/Launcher3).
- [raphtlw/Lawnchair](https://github.com/raphtlw/Lawnchair) - Minor fork of Lawnchair V2, with minor bug fixes.
- [Lawndesk](https://github.com/renzhn/Lawndesk) - Fork of Lawnchair V2; app-drawer-free launcher.
- [Librechair](https://gitlab.com/oldosfan/librechair) - Degoogled; fork of Lawnchair V2 & [Launcher3](https://github.com/amirzaidi/Launcher3).
- [OpenLauncher](https://github.com/OpenLauncherTeam/openlauncher) - Launcher that aims to be a powerful and community driven project.
- [Taskbar](https://github.com/farmerbb/Taskbar) - A PC-style Launcher for Android.

## Photos

- [DNG Processor](https://github.com/amirzaidi/DNGProcessor) - Bring out the best in your phone's camera. It waits for new RAW images captured using a supported camera app, and then processes them in the background. Works well with Open Camera and usually gives better results than Google Camera ports.
- [Phimp.me](https://github.com/fossasia/phimpme-android) - Photo Imaging and Picture Editor.
- [Simple Gallery](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/) - A premium app for managing and editing your photos, videos, GIFs without ads.

## Productivity

- [Acode](https://acode.foxdebug.com/) - A powerful code editor for Android.
- [Fokus](https://github.com/asayah-san/fokus-android) - Reminder app for tasks and events tailored specifically for students.
- [Goodtime](https://github.com/adrcotfas/Goodtime) - A minimalist but powerful productivity timer designed to keep you focused and free of distractions.
- [Notally](https://github.com/OmGodse/Notally) - A minimalist notes app with a material design.
- [Simple Notes](https://www.simplemobiletools.com/notes/) - Create plain text notes or checklists quickly.
- [Tasks](https://tasks.org/) - A To-Do list app based on Astrid To-Do list app.

## Streaming

- [NewPipe](https://newpipe.schabi.org/) - Lightweight Google-free YouTube client.
- [SkyTube](https://skytube-app.com/) - An open-source YouTube™ app for Android™.
- [Twire](https://github.com/twireapp/Twire) - An Open Source, AD-Free Twitch browser and stream player for Android. Supports VODs with chat replay, custom emotes (BTTV and FFZ) and Picture in Picture mode. A fork of Pocket Plays for Twitch.

## VPN

- [ProtonVPN](https://protonvpn.com/) - Operated by Swiss company Proton Technologies; [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)! :) ([Source Code](https://github.com/ProtonVPN/android-app)).
- [RiseupVPN](https://riseup.net/en/vpn) - Community-driven; based in Washington, USA (**WARNING: [Five Eyes](https://en.wikipedia.org/wiki/Five_Eyes)**).
- [Mullvad](https://mullvad.net/en/) - Mullvad was founded in 2009 purely with the ambition of upholding the universal right to privacy – for you, for us, for everyone. And not only that, we want to make Internet censorship and mass surveillance ineffective.

## Others

- [Codec Info](https://github.com/Parseus/codecinfo) - Detailed listing of multimedia codecs on your Android device - with no ads!
- [CPU Info](https://github.com/kamgurgul/cpu-info) - Provides information about device hardware and software.
- [EtchDroid](https://f-droid.org/en/packages/eu.depau.etchdroid/) - Helps you with writing ISO images and creating bootable USB drives, no root required.
- [Exodus](https://github.com/Exodus-Privacy/exodus-android-app) - Tracker finder for a particular Android application.
- [LabCoat](https://gitlab.com/Commit451/LabCoat) - GitLab client for Android.
- [OctoDroid](https://github.com/slapperwan/gh4a) - Github client for Android.
- [OpenHub](https://github.com/ThirtyDegreesRay/OpenHub) - An open-source GitHub Android client app, faster and concise.
- [OsmAnd](https://osmand.net/) - An open-source map and navigation app for Android (and iOS) that uses OpenStreetMap map and Wikipedia databases.
- [StreetComplete](https://f-droid.org/en/packages/de.westnordost.streetcomplete/) - OpenStreetMap surveyor app. This app finds incomplete and extendable data in your vicinity and displays it on a map as markers. Each of those is solvable by answering a simple question to complete the info on site.
- [Termux](https://github.com/termux/termux-app) - Termux is an Android terminal emulator and Linux environment app.

## Tools

- [AFWall+](https://f-droid.org/en/packages/dev.ukanth.ufirewall/) - Control network traffic (Requires root).
- [Download Navi](https://github.com/TachibanaGeneralLaboratories/download-navi) - A Free and Open Source software download manager.
- [Material Files](https://github.com/zhanghai/MaterialFiles) - Material Design file manager (Root permission is optional).
- [MoeList](https://github.com/axiel7/MoeList) - Unofficial MAL (MyAnimeList) client. With this app you can easily track, rate, organize and discover new Anime & Manga.
- [ScreenCam](https://gitlab.com/vijai/screenrecorder) - Lightweight and functional screen recorder.
- [Simple File Manager](https://f-droid.org/en/packages/com.simplemobiletools.filemanager.pro/) - A simple file manager for browsing and editing files and directories.
- [TrackerControl](https://f-droid.org/en/packages/net.kollnig.missioncontrol.fdroid/) - TrackerControl allows to monitor and control hidden data collection in apps.

## Modding

> **NOTE: This category is for those that want to modify their Android system. Be warned that some of the rooted apps may result in a bootloop.**

- [/d/gapps](https://f-droid.org/en/packages/org.droidtr.deletegapps/) - Delete/disable GApps (root permission is optional but recommended).
- [App Manager](https://github.com/MuntashirAkon/AppManager) - A full-featured package manager and viewer for Android (Root permission is optional).
- [Island](https://island.oasisfeng.com/) - By the creator of [Greenify](https://forum.xda-developers.com/apps/greenify); uses the "Work Profile" feature of Android to create a sandbox environment to clone apps and isolate them ([Source Code](https://github.com/oasisfeng/island)) (Root permission is optional).
- [Insular](https://gitlab.com/secure-system/Insular) - Fork of [Island](#island) with removed proprietary Google Play libraries.
- [Linux Deploy](https://github.com/meefik/linuxdeploy) - Install and run GNU/Linux on Android (Requires root).
- [Magisk Manager](https://github.com/topjohnwu/Magisk) - Front-end to [Magisk](https://magisk.me/) (Require root).
- [microG](https://microg.org/) - FOSS alternative to Google Play Services to run apps that are require to have Google Play Services installed. **(WARNING: microG Services trip [SafetyNet](https://developers.google.com/android/reference/com/google/android/gms/safetynet/SafetyNet) and do your research before proceeding!)** Read the [unofficial microG wiki](https://old.reddit.com/r/MicroG/wiki/index) to know how to install ([Source Code](https://github.com/microg)).
- [Nethunter-App](http://store.nethunter.com/en/packages/com.offsec.nethunter/) - This is the next-gen Nethunter app, which acts as an installer, updater,
  and interface for the Kali Linux chroot. Please note that it requires [Nethunter Terminal](http://store.nethunter.com/en/packages/com.offsec.nhterm/) to work.
- [Shelter](https://github.com/PeterCxy/Shelter) - Leveraging the “Work Profile” feature of Android to provide an isolated space that you can install or clone apps into (It has a few compatibility issues with Android 10+ for now but you can still use it if you like).
- [Smartpack-Kernel Manager](https://github.com/SmartPack/SmartPack-Kernel-Manager/) - Fork of [Kernel Adiutor](https://play.google.com/store/apps/details?id=com.grarak.kerneladiutor&hl=en) with a set of additional features (Require root).
- [VirtualXposed](https://f-droid.org/en/packages/io.va.exposed/) - Use Xposed without root, unlock the bootloader or modify system image (Supports Android 5.0~9.0).

## Contributing

Want to contribute? Feel free to fork the project and send merge requests when you are done. Be sure to follow [these rules](Contributing.md).

I will check it out and will decide if I should accept it or not. If you do not want to fork the project, consider [opening an issue](https://github.com/TheEvilSkeleton/free-and-open-source-android-apps/issues) and suggest what application you want me to install.

## Footnotes

### Deprecated contents

To see content that is deprecated, lacking documentation or proprietary but still useful go to [DeprecatedContents](DeprecatedContents.md)

### Other Git repositories

[Ashpex / Android FOSS Apps](https://gitlab.com/Ashpex/android-FOSS-apps)

[Mybridge / amazing-android-apps](https://github.com/Mybridge/amazing-android-apps)

[pcqpcq / open-source-android-apps](https://github.com/pcqpcq/open-source-android-apps)

[unicodedeveloper / awesome-opensource-apps](https://github.com/unicodeveloper/awesome-opensource-apps#android)

[samedamci / FOSS_Stuff](https://github.com/samedamci/FOSS_Stuff)

### External sources

[Android Authority](https://www.androidauthority.com/best-open-source-apps-for-android-861665/)

[Beebom](https://beebom.com/open-source-apps-android/)

[FossBytes](https://fossbytes.com/best-free-open-source-apps-android/)

[TechWiser](https://techwiser.com/open-source-android-apps/)

### Mirrors

- Codeberg: https://codeberg.org/LinuxCafeFederation/awesome-android
- GitLab: https://gitlab.com/linuxcafefederation/awesome-android
- GitHub: https://github.com/LinuxCafeFederation/awesome-android
